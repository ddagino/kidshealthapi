<?php
/**
 * @file
 * Contains npd\KidsHealth\FTPClient.
 */

namespace npd\KidsHealth;

use Touki\FTP\Connection\Connection;
use Touki\FTP\FTPWrapper;
use Touki\FTP\FTPFactory;

class FTPClient {

  private $username;
  private $connection;
  private $instance;
  private $wrapper;
  private $files;

  /**
   * Open a connection to the FTP server.
   *
   * @param string $hostname
   *   Hostname or IP of the FTP server.
   * @param string $username
   *   Name of the FTP user authorized to connect to the server.
   * @param string $password
   *   Password of the FTP user.
   */
  public function __construct($hostname, $username, $password) {
    $this->username = $username;

    $factory = new FTPFactory();
    $this->connection = new Connection($hostname, $username, $password);
    $this->connection->open();

    $this->wrapper = new FTPWrapper($this->connection);
    $this->wrapper->pasv(TRUE);

    $factory->setWrapper($this->wrapper);

    $this->instance = $factory->build($this->connection);
  }

  public function getListOfFiles($subdir = '') {
    $directory = $this->wrapper->pwd() . $subdir ?: '';
    $files = $this->wrapper->rawlist($directory);
    // Remove the "total xxxxxx" line.
    array_shift($files);
    foreach ($files as $file) {
      $unix_filetype = substr($file, 0, 1);
      if ($unix_filetype != 'd') {
        $this->files[] = $this->parseFileName($file);
      }
    }

    return $this->files;
  }

  private function parseFileName($file) {
    $parts = preg_split("/\s+/", $file);
    return $parts[8];
  }

  /**
   * Save an archive file to the local disk.
   *
   * @param string $filename
   *   The path to the remote file relative to the current directory.
   * @param string $local_directory
   *   Path to the local storage directory for the archive files.
   *
   * @return string
   *   The path to the local file.
   */
  public function getFile($filename, $local_directory) {
    //$tempfile = tempnam(sys_get_temp_dir(), $filename);
    $local_file = $local_directory  . '/' . $filename;
    $handle = fopen($local_file, 'w+');
    $file = $this->instance->findFileByName($filename, $this->instance->getCwd());
    if ($this->instance->download($handle, $file)) {
      return $local_file;
    }
    return FALSE;
  }

  public function isConnected() {
    return $this->connection->isConnected();
  }

  public function closeConnection() {
    $this->connection->close();
  }

}
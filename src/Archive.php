<?php
/**
 * @file
 * Contains class \npd\KidsHealth\Archive.
 */

namespace npd\KidsHealth;

use wapmorgan\UnifiedArchive\UnifiedArchive;

class Archive {

  private $filename;
  private $baseFilename;
  private $archive;

  public function __construct($filename) {
    $this->filename = $filename;
    $this->baseFilename = $this->getFileBaseName();
    $this->archive = UnifiedArchive::open($filename);
  }

  private function getFileBaseName() {
    if (preg_match('/xmlfeed_\w+-\d{4}_\d{2}_\d{2}/', $this->filename, $matches)) {
      return $matches[0];
    }
  }

  public function getXml() {
    $base_filename = $this->getFileBaseName();
    if (!empty($base_filename)) {
      return $this->archive->getFileContent('./' . $base_filename . '.xml');
    }
  }

  public function getImageIndex() {
    $image_index_filename = './xmlfeed_image_index_lic500-2014_10_09.txt';
    return $this->archive->getFileContent($image_index_filename);
  }

  public function getImages() {
    // ./xmlfeed_images_lic500-2014_10_09.tar
    //return $this->archive->getFileContent($images_tar);
  }

  public function getCss() {
    //return $this->archive->getFileContent('wellnessCenterMain.css');;
  }

}
<?php
/**
 * @file
 * Contains class npd\KidsHealth\ArchiveManager.
 */

namespace npd\KidsHealth;

use \wapmorgan\UnifiedArchive\UnifiedArchive;
use \npd\KidsHealth\Archive;
use \npd\KidsHealth\FTPClient;

class ArchiveManager {

  private $ftpClient;
  private $localDirectory;
  private $archives;

  public function __construct($ftp_client, $local_directory = '') {
    $this->ftpClient = $ftp_client;
    $this->localDirectory = empty($local_directory) ? sys_get_temp_dir() : $local_directory;
  }

  public function loadArchivesFromData($archives) {
    foreach ($archives as $filename => $local_uri) {
      if ($this->archiveExists($filename)) {
        $this->archives[$filename] = new Archive($local_uri);
      }
    }
  }

  public function getNewArchive() {
    $new_archives = $this->ftpClient->getListOfFiles();
    foreach ($new_archives as $filename) {
      return $this->getArchive($filename);
    }
  }

  public function getArchive($filename) {
    // Check the temp directory for the file, skip download if they match.
    if (isset($this->archives[$filename])) {
      return $this->archives[$filename];
    }
    if (!$this->archiveExists($filename)) {
      $this->ftpClient->getFile($filename, $this->localDirectory);
    }
    $archive = new Archive($filename);
    $this->archives[$filename] = $archive;
    return $archive;
  }

  private function archiveExists($filename) {
    $files = scandir($this->localDirectory, SCANDIR_SORT_DESCENDING);
    foreach ($files as $file) {
      if (strpos($file, $filename) === 0) {
        // Check for partial downloads.
        /* @todo test later.
        if (filesize($file) < $this->ftpClient->wrapper->size($filename)) {
          return FALSE;
        }
        */
        return TRUE;
      }
    }
  }

}
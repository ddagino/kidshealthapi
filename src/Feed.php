<?php
/**
 * @file
 * Contains class \npd\KidsHealth\Feed.
 */

namespace npd\KidsHealth;

use npd\KidsHealth\Archive;

class Feed {

  private $xml;

  public function __construct($xml) {
    $this->xml = @simplexml_load_string($xml);
  }

  public function getCategories() {
    return $this->xml->xpath('//category');
  }

}